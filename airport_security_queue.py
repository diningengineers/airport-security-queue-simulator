__author__ = 'dining engineers'

import numpy as np


class CheckinQueue:
    """
        Passenger queue is a dictionary data structure
        the key is the passenger exit time and value is the passenger class
    """
    SERVICE_TIME = 20
    MAX_SERVING_AGENTS = 8
    WINDOW_SIZE = 40

    def __init__(self, n_agents, var_more_threshold, var_less_threshold, seed=501):
        self.queue = {}
        self.fixed_agents = n_agents != 0  # if agents == 0 => variable number based on our policy
                                           # if agents != 0 keep that number
        # start from 1 if variable otherwise keep the value provided
        self.serving_agents = 1 if not self.fixed_agents else n_agents
        self.window_wait_time = np.zeros((1, self.WINDOW_SIZE))
        self.more_threshold = var_more_threshold
        self.less_threshold = var_less_threshold
        np.random.seed(seed)   # Seeded to allow replication.

    def remove_passenger_from_queue(self, current_time, flight_checkin_times):
        passengers_removed = []
        key_to_remove = [i for i in self.queue.keys() if i <= current_time]
        flight_lost = False
        for i in key_to_remove:
            p = self.queue[i]
            if p.forecast_exit_time < current_time:
                del self.queue[i]
                passengers_removed.append(p)
            else:
                # have to exit but flight departed so get discarded
                if current_time >= flight_checkin_times[p.flight_number-1]:
                    del self.queue[i]
                    p.lost = True
                    passengers_removed.append(p)
                    flight_lost = True

                # print "tick ", current_time, p.entry_time, p.forecast_exit_time
        if flight_lost:
            self.update_forecast_exit_time()
        return passengers_removed

    def add_passenger_in_queue(self, passenger):

        if passenger.entry_time in self.queue.keys():
            return False

        # assign service time
        passenger.service_time = self.gen_service_time()

        # take all the passenger in queue before the passenger to add
        passengers_before = [i for i in self.queue.keys() if i <= passenger.entry_time]
        # print passenger.entry_time, passengers_before,  [i for i in self.queue.keys()]
        if len(passengers_before) == 0:
            passenger.forecast_exit_time = passenger.entry_time + passenger.service_time
        else:
            # take the one passenger before the one to add
            prec_passenger = self.queue[max(passengers_before)]
            passenger.forecast_exit_time = prec_passenger.forecast_exit_time + passenger.service_time

        if len(self.queue) == 0:
            wait_time = passenger.forecast_exit_time - passenger.entry_time
            self.window_wait_time = np.ones((1, self.WINDOW_SIZE)) * wait_time

        # add in the actual dictionary
        self.queue[passenger.entry_time] = passenger

        if not self.fixed_agents:  # we are using the variable number of serving agents
            wait_time = passenger.forecast_exit_time - passenger.entry_time
            self.window_wait_time = np.roll(self.window_wait_time, 1)
            self.window_wait_time[0] = wait_time
            self.check_queue_service_time(np.mean(self.window_wait_time))

        return True

    def __len__(self):
        return len(self.queue)

    def check_queue_service_time(self, window_mean_waiting_time):
        if window_mean_waiting_time > self.more_threshold and self.serving_agents < self.MAX_SERVING_AGENTS:
            # print " INCREASE SERVING AGENTS"
            self.serving_agents += 1
            self.update_forecast_exit_time()
        elif self.serving_agents > 1 and window_mean_waiting_time < self.less_threshold:
            # print " REDUCE SERVING AGENTS"
            self.serving_agents -= 1
            self.update_forecast_exit_time()

    def update_forecast_exit_time(self):
        # assume that keys are in order
        # Redo the forecast assigment
        for index, p in enumerate(self.queue.keys()):
            if index == 0:
                # the first one
                self.queue[p].service_time = self.gen_service_time()
                self.queue[p].forecast_exit_time = self.queue[p].entry_time + self.queue[p].service_time
                prec = self.queue[p]
            else:
                # precedent passenger in queue
                self.queue[p].service_time = self.gen_service_time()
                if self.queue[p].entry_time < prec.forecast_exit_time:
                    self.queue[p].forecast_exit_time = prec.forecast_exit_time + self.queue[p].service_time
                else:
                    self.queue[p].forecast_exit_time = self.queue[p].entry_time + self.queue[p].service_time
                assert(self.queue[p].entry_time < self.queue[p].forecast_exit_time)
                prec = self.queue[p]

        for index, p in enumerate(self.queue.keys()[-self.WINDOW_SIZE:]):
            if index == 0:
                wait_time = self.queue[p].forecast_exit_time - self.queue[p].entry_time
                self.window_wait_time = np.ones((1, self.WINDOW_SIZE)) * wait_time
            else:
                wait_time = self.queue[p].forecast_exit_time - self.queue[p].entry_time
                self.window_wait_time = np.roll(self.window_wait_time, 1)
                self.window_wait_time[0] = wait_time

    def gen_service_time(self):
        #TODO da fare meglio
        return np.random.exponential(self.SERVICE_TIME/float(self.serving_agents))
        # return SERVICE_TIME/float(self.serving_agents)

    def cost(self):
        # alpha = 10
        # beta = 1/float(20)
        # cost = alpha * self.serving_agents + beta * len(self.queue)*(SERVICE_TIME/float(self.serving_agents))
        return self.SERVICE_TIME/float(self.serving_agents) #cost

    def window_waiting_time(self):
        return np.mean(self.window_wait_time)