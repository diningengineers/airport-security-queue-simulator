from itertools import starmap

__author__ = 'dining engineers'

import csv
import os
import datetime
import numpy as np
import matplotlib.pyplot as plt
import scipy
from scipy.stats import norm
from scipy.stats import rayleigh
from pylab import *
import random
import math

show_graph = True

GENERATION_TYPES = ['uniform']
PDF_TYPES = ['gaussian', 'rayleigh']

def normalize_vector(v):
    return (v-np.min(v))/(np.max(v)-np.min(v))


def normalize_vector(v, min, max):
    return (v-min)/(max-min)


def gen_flights_times(first_flight, last_flight, total_flight, gen_type):
    if gen_type == 'uniform':
        val = linspace(first_flight, last_flight, total_flight)
    return val


def gen_flight_queue_pdf(flight_checkin_times, sim_end_time, pdf_type):
    _flights_pdf = []

    if pdf_type == 'gaussian':
        # sigma = 15*60       # [s]
        sigma = random.randint(15, 30)*60
        sigma_norm = sigma/sim_end_time  # 15*60       # [s]
        for f in flight_checkin_times:
            _flights_pdf.append(scipy.stats.norm(f-3*sigma_norm, sigma_norm))

    if pdf_type == 'rayleigh':
        scale = (1.0/len(flight_checkin_times))/2.5
        for f in flight_checkin_times:
            rv = scipy.stats.rayleigh(scale=scale)
            _flights_pdf.append(rv)

        #rv.pdf((-0.79+0.8))
    return _flights_pdf


def get_flight_pdf(f, x, flight_checkin_time, pdf_type):

    if pdf_type == 'gaussian':
        return f.pdf(x)
    if pdf_type == 'rayleigh':
        return f.pdf(-x+flight_checkin_time)


def read_csv(url):
    input_entry = []
    with open(url, 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
        for row in spamreader:
            input_entry.append(row)
    return input_entry


def write_csv(url, dataset, flights):
    filename = url+'.csv'
    if not os.path.exists(os.path.dirname(filename)):
        os.makedirs(os.path.dirname(filename))

    f = open(filename, 'w')
    writer = csv.writer(f)
    for i in dataset:
        writer.writerow(i)
    f.close()

    filename = url+'-info.csv'
    if not os.path.exists(os.path.dirname(filename)):
        os.makedirs(os.path.dirname(filename))
    f = open(filename, 'w')
    writer = csv.writer(f)
    writer.writerow(flights)
    f.close()


if __name__ == "__main__":

    print "********************************************"
    print "\tAirport security queue dataset generator"
    print "********************************************"

    for number_flight in range(5, 21):

        print "Generating queue with ", number_flight, " flights in a day"

        for pdf_types in PDF_TYPES:

            print "\t Generating queue with PDF ", pdf_types, "..."

            for gen_type in GENERATION_TYPES:

                # generate for each of 5 files (data1, data2 ...)
                for file_number in range(1, 6):

                    print "\t\tWorking on file: ", file_number

                    # read entry time data from file
                    file_input_times = read_csv('./data/data'+str(file_number)+'.csv')

                    # data to write in .csv file <entry_time [s]>, <normalized_entry_time>, <flight_number>
                    file_output_info = np.zeros((len(file_input_times), 3))

                    # parse time data format
                    start_time = datetime.datetime.strptime(file_input_times[0][0], "%Y-%m-%dT%H:%M:%S.%f+02:00")
                    for i, entry in enumerate(file_input_times):
                        entry_time = datetime.datetime.strptime(entry[0], "%Y-%m-%dT%H:%M:%S.%f+02:00")
                        file_output_info[i, 0] = (entry_time-start_time).total_seconds()

                    # end time (so when the last flight depart) is prolonged 10 min after last person arrive in queue
                    end_time = file_output_info[-1, 0] + 10*60

                    # generate flight checkin closure time
                    # we choose the start time of the first flight 1 hour after first person in queue
                    flight_checkin = gen_flights_times(60*60, end_time, number_flight, gen_type)

                    # normalize intervals between [0,1]
                    file_output_info[:, 1] = normalize_vector(file_output_info[:, 0], 0, end_time)
                    flight_checkin_norm = (flight_checkin - (file_output_info[0, 0]))/(end_time-(file_output_info[0, 0]))


                    # generate pdf for each entry to be in a certain flight
                    # we choose a gaussian with:
                    #   mean = checkin closure time - 2*sigma
                    #   sigma = 15 min
                    # so the 95% of passenger have done the security check in time
                    flights_pdf = gen_flight_queue_pdf(flight_checkin_norm, end_time, pdf_type=pdf_types)
                    # For each entry random associated flight based on PDF calculated
                    for i in range(len(file_output_info)):
                        # take P from each gaussian and normalize
                        tmp = np.zeros((1, number_flight))
                        for j in range(len(flights_pdf)):
                            tmp[0, j] = get_flight_pdf(flights_pdf[j], file_output_info[i, 1], flight_checkin_norm[j], pdf_types)
                            # print tmp[0, j]
                                # flights_pdf[j].pdf(file_output_info[i, 1])

                        if np.all(tmp == 0):
                            file_output_info[i, 2] = number_flight
                        else:
                            tmp = (tmp-np.min(tmp))/(np.max(tmp)-np.min(tmp))
                            # error check
                            if(np.isnan(tmp).any()):
                                print tmp, file_output_info[i], i, flight_checkin_norm, file_output_info[i, 1]

                            tmp = tmp/sum(tmp)

                            flight = range(1, number_flight+1)
                            # do actual random with discrete prob
                            gen_flight_association = scipy.stats.rv_discrete(values=(flight, tmp))
                            # print tmp, i
                            file_output_info[i, 2] = gen_flight_association.rvs()


                    # SAVE generated data
                    # file_output = open('./data/gen/data'+str(file_number)+'-'+str(number_flight)+'.csv', 'w')
                    # writer = csv.writer(file_output)
                    # for i in file_output_info:
                    #         writer.writerow(i)
                    # file_output.close()
                    #
                    # file_output = open('./data/gen/data'+str(file_number)+'-'+str(number_flight)+'-info.csv', 'w')
                    # writer = csv.writer(file_output)
                    # writer.writerow(flight_checkin)
                    # file_output.close()
                    folder_path = './data/gen/'+gen_type+'/'+pdf_types+'/'+str(number_flight)+'-flights/'
                    if not os.path.exists(os.path.dirname(folder_path)):
                            os.makedirs(os.path.dirname(folder_path))

                    write_csv(folder_path+'data'+str(file_number)+'-'+str(number_flight), file_output_info, flight_checkin)

                    # Plot flight PDF
                    if show_graph:
                        # define the points to plot the gaussian distributions
                        x = linspace(0, flight_checkin_norm[-1], 2000)

                        # fig = plt.figure()
                        # entry_p_for_flight = np.zeros((len(file_output_info), number_flight))
                        colors = iter(cm.rainbow(np.linspace(0, 1, number_flight)))
                        for i, p in enumerate(flights_pdf):
                            col = next(colors)
                            pdf = get_flight_pdf(p, x, flight_checkin_norm[i], pdf_types)
                            plt.plot(x, pdf, color=col)
                            plt.plot([flight_checkin_norm[i], flight_checkin_norm[i]], [0, max(pdf)], color=col)

                        plt.title('Flight PDF queue distribution')
                        plt.xlabel('t')
                        plt.ylabel('P')
                        plt.grid(True)
                        plt.ylim((0, max(pdf)))
                        plt.savefig(folder_path+'data'+str(file_number)+'-'+str(number_flight)+'.svg')

                        plt.clf()
                        # plt.show()

                        plt.title('Queue popolation for day '+str(file_number))
                        plt.xlabel('t sim')
                        plt.ylabel('number of passengers')
                        plt.grid(True)
                        plt.ylim((0, 70))

                        plt.hist(file_output_info[:, 0], 200, color='k')
                        plt.savefig(folder_path+'data'+str(file_number)+'-histogram.svg')

                        plt.clf()

                        # for i in flight_checkin_norm:
                        # plt.bar(bins, hist)
                        # plt.show()

    print "Generation completed"


