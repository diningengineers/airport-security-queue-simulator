from datetime import datetime

__author__ = 'dining engineers'

from airport_security_queue import *
from airport_security_passenger import *

import numpy as np
import csv
from numpy import genfromtxt
import os


SIMULATION_STEP = 1


def airport_security_queue_simulator(agents, file_n, generation_type, pdf, n_flights,
                                     var_more_threshold=5*60, var_less_threshold=1*60,
                                     save_csv_res=True):

    # Discard with high cost, simulation not in bound
    if var_less_threshold > var_more_threshold or var_more_threshold < 0 or var_less_threshold < 0:
        return 500.

    # OPTIONS ############
    number_serving_agents = agents     # set the init serving agents, if 0 autobalancing otherwise fixed
    file_number = file_n
    gen_type = generation_type
    pdf_types = pdf
    flights_number = n_flights
    #######################

    folder_path = './data/gen/'+gen_type+'/'+pdf_types+'/'+str(flights_number)+'-flights/'
    passenger_entry_times = genfromtxt(folder_path+'data'+str(file_number)+'-'+str(flights_number)+'.csv', delimiter=',')
    flight_checkin_times = genfromtxt(folder_path+'data'+str(file_number)+'-'+str(flights_number)+'-info.csv', delimiter=',')

    # define queue object
    checkin_queue = CheckinQueue(number_serving_agents, var_more_threshold, var_less_threshold)
    total_passengers = len(passenger_entry_times)
    simulation_result = np.zeros((total_passengers, 5))
    passenger_entered_in_queue = 0  #
    passenger_served = 0            # count passenger exited from queue
    number_of_switch = 0            # number of times we change the serving agents number
    flight_lost = 0
    queue_history = {}              # save queue history for each flight
    serving_history = []            # used only if variable serving agents
    queue_cost = []                 # used only if variable serving agents
    inactive_agents = []            # inactive agents per second
    window_wait_time = []

    # initialize a dict with many elements as flights_number
    # each element contains a list with the status of the queue at time i
    for i in range(0, flights_number+1):
        # nb: key 0 => global queue
        #     key > 0: queue of key flight
        queue_history[i] = []

    # START the simulation until all passengers are served
    current_time = 0                # simulation time
    while passenger_served < total_passengers:

        # REMOVE SERVED passenger from queue
        # check if at current time we need to remove passengers from the queue beacause:
        #   1) their flight already departed
        #   2) they have been served
        passenger_removed = checkin_queue.remove_passenger_from_queue(current_time, flight_checkin_times)

        # save the state of queue at time t = t-1
        for i in range(0, flights_number+1):
            if current_time == 0:
                queue_history[i].append(0)
            else:
                queue_history[i].append(queue_history[i][-1])

        # ADD NEW ELEMENTS
        # Check passenger that should enter in queue at current_time
        # add new passenger in queue if entry time < current time
        while passenger_entered_in_queue < total_passengers and passenger_entry_times[passenger_entered_in_queue, 0] <= current_time:
            # create a new passenger and insert the passenger in the queue
            passenger = Passenger(passenger_entry_times[passenger_entered_in_queue, 0], passenger_entry_times[passenger_entered_in_queue, 2])
            checkin_queue.add_passenger_in_queue(passenger)
            # save state of queue for p.flight
            queue_history[int(passenger.flight_number)][-1] += 1
            # save overall queue info
            queue_history[0][-1] += 1
            passenger_entered_in_queue += 1

        flight_lost_at_current_time = 0
        # SAVE REMOVED PASSENGERS
        for p in passenger_removed:

            # entry time
            simulation_result[passenger_served, 0] = np.around(p.entry_time, decimals=3)
            # service time
            simulation_result[passenger_served, 1] = np.around(p.service_time, decimals=3)
            # forecast exit time
            simulation_result[passenger_served, 2] = np.around(p.forecast_exit_time, decimals=3)
            # flight associated departure
            simulation_result[passenger_served, 3] = np.around(flight_checkin_times[p.flight_number-1], decimals=3)
            # wait time in queue
            simulation_result[passenger_served, 4] = np.around(p.forecast_exit_time - p.entry_time, decimals=3)

            # increase the number of passenger served
            passenger_served += 1

            # keep track of the passenger that exited the queue that have lost the flight
            if p.lost:
                flight_lost += 1
                flight_lost_at_current_time += 1

            # update queue history status
            # for the flight history
            queue_history[int(p.flight_number)][-1] -= 1
            # for the overall history
            queue_history[0][-1] -= 1

        # update the history of the serving agents at this simulation time
        serving_history.append(checkin_queue.serving_agents)
        # save the mean cost to serve a passenger at this simulation time
        queue_cost.append(checkin_queue.cost())
        if len(checkin_queue) < checkin_queue.serving_agents:
            inactive_agents.append(checkin_queue.serving_agents - len(checkin_queue))
        else:
            inactive_agents.append(0)

        # if we are changing the number of serving agents
        if not checkin_queue.fixed_agents:
            window_wait_time.append(checkin_queue.window_waiting_time())
            # check and keep track if the number of serving agents changed at this sim time
            if len(serving_history) > 2 and serving_history[-2] < checkin_queue.serving_agents:
                ## we are adding a SA
                # we double the number since there is a higher cost in adding a SA than removing one
                number_of_switch += 1
            elif len(serving_history) > 2 and serving_history[-2] > checkin_queue.serving_agents:
                ## we are removing a SA
                number_of_switch += 1

        # increase simulation time
        current_time += SIMULATION_STEP

    ########### SAVE SIMULATION RESULTS #######################################
    if save_csv_res:
        res_folder = './data/results/'
        if not os.path.exists(os.path.dirname(res_folder)):
            os.makedirs(os.path.dirname(res_folder))
        file_output = open(res_folder+'simulation-summary-results.csv', 'a')
        writer = csv.writer(file_output, delimiter='\t')
        writer.writerow([file_number, flights_number, flight_lost, total_passengers,
                         np.around(float(flight_lost*100)/total_passengers, decimals=3),
                         'var' if number_serving_agents == 0 else str(number_serving_agents),
                         pdf_types])
        file_output.close()

        if not checkin_queue.fixed_agents:
            folder_output = './data/results/'+gen_type+'/'+pdf_types+'/'+str(flights_number)+'-flights/var-agents/day-'+str(file_number)+'/'
        else:
            folder_output = './data/results/'+gen_type+'/'+pdf_types+'/'+str(flights_number)+'-flights/'+str(number_serving_agents)+'-agents/day-'+str(file_number)+'/'

        if not os.path.exists(os.path.dirname(folder_output)):
            os.makedirs(os.path.dirname(folder_output))

        # SAVE SiM RESULTS
        file_output = open(folder_output+'data'+str(file_number)+'-'+str(flights_number)+'-results.csv', 'w')
        writer = csv.writer(file_output, delimiter='\t')
        for i in simulation_result:
            writer.writerow(i)
        file_output.close()

        # SAVE QUEUE HISTORY
        for i in range(0, flights_number+1):
            f = 'all' if i == 0 else str(i)
            file_output = open(folder_output+'queue-history-of-'+f+'.csv', 'w')
            writer = csv.writer(file_output, delimiter='\t')
            for index, j in enumerate(queue_history[i]):
                writer.writerow([index, j])
            file_output.close()

        # SAVE SERVING HISTORY
        if not checkin_queue.fixed_agents:
            file_output = open(folder_output+'serving-agents.csv', 'w')
            writer = csv.writer(file_output, delimiter='\t')
            for index, j in enumerate(serving_history):
                writer.writerow([index, j])
            file_output.close()

        # SAVE W_Times
        file_output = open(folder_output+'mean_wait_times.csv', 'w')
        writer = csv.writer(file_output, delimiter='\t')
        for index, j in enumerate(window_wait_time):
                writer.writerow([index, j])
        file_output.close()

    ############### SIMULATION COST #############################

    # SERVING HISTORY COST
    serving_cost = np.array(serving_history)
    inactive_agents_ratio = np.mean(np.divide(inactive_agents, serving_cost))
    # normalize
    serving_cost = (serving_cost - 1) / float(checkin_queue.MAX_SERVING_AGENTS - 1)
    # if we are changing the number of serving agents
    sa_cost = np.mean(serving_cost)

    # QUEUE MEAN SERVING TIME COST
    queue_cost = np.array(queue_cost)
    # normalize
    queue_cost = (queue_cost - (checkin_queue.SERVICE_TIME/checkin_queue.MAX_SERVING_AGENTS)) / \
                 float(checkin_queue.SERVICE_TIME - float(checkin_queue.SERVICE_TIME/checkin_queue.MAX_SERVING_AGENTS))
    q_cost = np.mean(queue_cost)

    # FLIGHT LOST COST
    flight_cost = flight_lost / float(total_passengers)
    # SWITCH COST
    # switch_cost = number_of_switch / float(total_passengers)  # TODO: or max float(current_time/SIMULATION_STEP))
    # sim_cost = (sa_cost + q_cost + flight_cost*4 + switch_cost*3)**2
    # possible new formula uncomment to use
    switch_cost = number_of_switch / float(current_time/SIMULATION_STEP)

    # Final Cost
    sim_cost = (sa_cost*10 + q_cost*0.1 + flight_cost*100 + switch_cost*300 + inactive_agents_ratio)**2

    ################### PRINT OUTPUT ####################
    ratio_lost = str(np.around(float(flight_lost*100)/total_passengers, decimals=3)) + "%"
    sa = 'var' if number_serving_agents == 0 else str(number_serving_agents)
    m_wait_t = str(np.mean(simulation_result[:, 4]))

    if number_serving_agents == 0:
        print "\t\t", file_number, "\t\t", flights_number, "\t\t\t", str(flight_lost).zfill(3), "\t\t\t", total_passengers, "\t\t",\
            ratio_lost, "\t\t\t", sa, "\t", m_wait_t, "\t\t", str(number_of_switch).zfill(3), "\t\t", \
            str(np.max(serving_history)), "\t\t\t", \
            "%11.6f" % (var_more_threshold), "\t", "%11.6f" % (var_less_threshold), "\t", np.round(sim_cost, 5),
    else:
        print "\t\t", file_number, "\t\t", flights_number, "\t\t", str(flight_lost).zfill(3), "\t\t", total_passengers, "\t\t",\
            ratio_lost, "\t\t\t", sa, "\t", m_wait_t, "\t", np.round(sim_cost, 5),
    #####################################################

    print "[ ", sa_cost*10, q_cost*0.1, flight_cost*100, switch_cost*300, inactive_agents_ratio, " ]"

    return sim_cost


def f(x0):
    '''
        function used by optimizer
    :param x0: initial point
    :return:
    '''
    a, b = x0
    return airport_security_queue_simulator(0, 5, 'uniform', 'gaussian', 10, a, b, False)

class RandomDisplacementBounds(object):
    """random displacement with bounds"""
    def __init__(self, xmin, xmax, stepsize=60):
        # print "init"
        self.xmin = xmin
        self.xmax = xmax
        self.stepsize = stepsize

    def __call__(self, x):
        """take a random step but ensure the new position is within the bounds"""
        print "new displacement"

        while True:
            print "new hopping"
            # this could be done in a much more clever way, but it will work for example purposes
            step = x + np.random.uniform(-self.stepsize, self.stepsize, np.shape(x))
            # xnew = np.round(xnew, 4)
            if np.all(step < self.xmax) and np.all(step > self.xmin) and step[0] > step[1]:
                break
        return step


_niter = 0

def print_fun(x, f, accepted):
    '''
    Print optimizer output
    :param x:
    :param f:
    :param accepted:
    :return:
    '''
    global _niter
    _niter+=1
    print(str(datetime.now())+' - '+str(_niter)+" - at minima %.4f accepted %d" % (f, int(accepted)) + " at point "+str(x))


def test():
    cost = []
    upper = [2, 3, 4, 5, 5, 6, 6, 6, 7, 7, 7, 8, 8, 3]
    lower = [1, 1, 2, 2, 1, 1, 2, 3, 1, 2, 3, 1, 2, 3]
    for a, b in zip(upper, lower):
        cost.append(airport_security_queue_simulator(0, 5, 'uniform', 'gaussian', 10, 60*a, 60*b, False))
    return cost


def test_prepare_plot(a, b, day, n_flight, max_sa, save=False):
    """
    take the mean cost with the best values found with basehopping opt
    :return:
    """
    print "test with: ", a, b
    for i in range(0, max_sa+1):
        airport_security_queue_simulator(i, day, 'uniform', 'gaussian', n_flight, a, b, save)


def optimize_basinhopping(method):
    x0 = np.array([360., 60.])     # Initial guess.
    from scipy import optimize
    np.random.seed(555)   # Seeded to allow replication.
    global _niter
    _niter = 0

    xmin = [0., 0.]
    xmax = [20*60., 20*60.]
    bnds = ((0., 20*60.), (0., 20*60.))
    # method can be:
    #  'Nelder-Mead' 'Powell' 'CG' 'BFGS' 'Newton-CG' 'Anneal' 'L-BFGS-B' 'TNC' 'COBYLA' 'SLSQP' 'dogleg' 'trust-ncg'
    minimizer_kwargs = {"method": method, "bounds": bnds} #"method": "BFGS"}
    # the bounds
    # define the new step taking routine and pass it to basinhopping
    take_step = RandomDisplacementBounds(xmin, xmax)
    res = optimize.basinhopping(f, x0, minimizer_kwargs=minimizer_kwargs,
                                niter=100, take_step=take_step, callback=print_fun)
    return res


if __name__ == "__main__":

    GENERATION_TYPES = ['uniform']
    PDF_TYPES = ['gaussian']#, 'rayleigh']

    # # delete previous simulations
    # folder_path = './data/results/'
    # if os.path.exists(os.path.dirname(folder_path)):
    #     shutil.rmtree(folder_path)

    print "***************************************"
    print "\tAirport Security Queue Simulator"
    print "***************************************"

    print "\t*Simulation end, Summary: "
    print "\t\tDay\t\tFlight\t\tFlight_Lost\t\tTot_P\t\tRatio_Lost\t\tSA\t\tMeanWaitTime\t\tSwitch\t\tMax_SA\t\tU\t\t\t\tL\t\t\tCOST"
    print "\t\t---------------------------------------------------------------------------------------------------------------------------------------"

    # # method can be:
    # #  'Nelder-Mead' 'Powell' 'CG' 'BFGS' 'Newton-CG' 'Anneal' 'L-BFGS-B' 'TNC' 'COBYLA' 'SLSQP' 'dogleg' 'trust-ncg'
    # res = optimize_basinhopping("BFGS")
    # res = optimize_basinhopping("BFGS")
    # res = test()
    # print res


    a = 661.10955927
    b = 58.99295119
    # airport_security_queue_simulator(0, 5, 'uniform', 'gaussian', 10, a, b, True)
    test_prepare_plot(a, b, 5, 10, 5, False)

