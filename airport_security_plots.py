from airport_security_queue import *
from airport_security_passenger import *

import numpy as np
import csv
from numpy import genfromtxt
import matplotlib.pyplot as plt
import os


SIMULATION_STEP = 1
# SERVICE_TIME = 20
GENERATION_TYPE = 'uniform'
PDF_TYPE = 'gaussian'


def plot_overall_agents_queue_length_over_time(flights_number, _day):
    print "\t\t\t\tplot_overall_agents_queue_length_over_time..."
    # with plt.xkcd():
    folder_output = './data/results/'+GENERATION_TYPE+'/'+PDF_TYPE+'/'+str(flights_number)+'-flights/'

    plt.title('Queue length over time')
    plt.xlabel('t [s]')
    plt.ylabel('#queue length')
    plt.grid(True)

    colors = iter(plt.cm.rainbow(np.linspace(0, 1, 5)))
    col = next(colors)
    data_plot = genfromtxt(folder_output+'var-agents/day-'+str(_day)+'/queue-history-of-all.csv', delimiter='\t')
    plt.plot(data_plot[:, 0], data_plot[:, 1], color=col, label='var agents')

    for i in range(1, 5):
        col = next(colors)
        data_plot = genfromtxt(folder_output+str(i)+'-agents/day-'+str(_day)+'/queue-history-of-all.csv', delimiter='\t')
        plt.plot(data_plot[:, 0], data_plot[:, 1], color=col, label=str(i)+' agents')

    plt.legend(loc=1)
    plt.savefig(folder_output+'overall_queue_length_over_time_day'+str(_day)+'.png', dpi=150)

    plt.clf()
    #
    # # REDRAW ANOTHER EXCLUDING 1 AGENT
    # plt.title('Queue length over time')
    # plt.xlabel('t sim')
    # plt.ylabel('queue length')
    # plt.grid(True)
    # colors = iter(plt.cm.rainbow(np.linspace(0, 1, 5)))
    # col = next(colors)
    # data_plot = genfromtxt(folder_output+'var-agents/day-'+str(_day)+'/queue-history-of-all.csv', delimiter='\t')
    # plt.plot(data_plot[:, 0], data_plot[:, 1], color=col, label='var agents')
    # col = next(colors)
    # for i in range(2, 5):
    #     col = next(colors)
    #     data_plot = genfromtxt(folder_output+str(i)+'-agents/day-'+str(_day)+'/queue-history-of-all.csv', delimiter='\t')
    #     plt.plot(data_plot[:, 0], data_plot[:, 1], color=col, label=str(i)+' agents')
    #
    # plt.legend(loc=1)
    # plt.savefig(folder_output+'overall_queue_length_over_time2.png', dpi=150)

    # plt.clf()


def plot_single_agent_queue_length_over_time(flights_number, _day, agent):
    # agent 0 == var
    print "\t\t\t\tplot_single_agent_queue_length_over_time..."
    label = 'var' if agent == 0 else str(agent)
    folder = './data/results/'+GENERATION_TYPE+'/'+PDF_TYPE+'/'+str(flights_number)+'-flights/'+label+'-agents/day-'+str(_day)+'/'

    plt.title('Queue length over time - day '+str(_day)+' - '+label+' agents')
    plt.xlabel('t [s]')
    plt.ylabel('#queue length')
    plt.grid(True)

    # DRAW QUEUE HISTORY
    data_plot = genfromtxt(folder+'queue-history-of-all.csv', delimiter='\t')
    plt.bar(data_plot[:, 0], data_plot[:, 1], color='k')

    # DRAW ARROWS
    # ARROWS IF SERVING > or <
    serving_data_plot = genfromtxt(folder+'serving-agents.csv', delimiter='\t')
    changes = serving_data_plot[:, 1]
    changes = np.roll(changes - np.roll(changes, -1), 1)
    changes[0] = 0
    serv_times_to_plot_minus = serving_data_plot[:, 0][changes > 0]
    serv_times_to_plot_plus = serving_data_plot[:, 0][changes < 0]
    h = plt.ylim()[1]*0.9
    l = h*0.08
    max_time = np.max(data_plot[:, 0])
    min_time = np.min(data_plot[:, 0])
    for i in serv_times_to_plot_plus:
        if max_time > i > min_time:
            plt.arrow(i, h, 0, l, head_width=l*80, head_length=l/10, fc='k', color='b', shape='full', length_includes_head=True)
    for i in serv_times_to_plot_minus:
        if max_time > i > min_time:
            plt.arrow(i, h, 0, -l, head_width=l*80, head_length=l/10, fc='k', color='r', shape='full', length_includes_head=True)

    folder_output = './data/results/'+GENERATION_TYPE+'/'+PDF_TYPE+'/'+str(flights_number)+'-flights/'
    plt.savefig(folder_output+'single_queue_length_over_time_agent_'+label+'_day'+str(_day)+'.png', dpi=150)
    plt.clf()


def plot_overall_queue_waiting_times_over_times(flights_number, day_number):
    print "\t\t\t\tplot_overall_queue_waiting_times_over_times..."
    # folder_output = './data/results/'+GENERATION_TYPE+'/'+PDF_TYPE+'/'+str(flights_number)+'-flights/'
    folder_output = './data/results/'+GENERATION_TYPE+'/'+PDF_TYPE+'/'+str(flights_number)+'-flights/'

    plt.title('Queue waiting time - day '+str(day_number))
    plt.xlabel('t [s]')
    plt.ylabel('waiting time [s]')
    plt.grid(True)

    # HISTOGRAM
    data_plot = genfromtxt(folder_output+'var-agents/day-'+str(day_number)+'/data'+str(day_number)+'-'+str(flights_number)+'-results.csv', delimiter='\t')
    # plt.bar(data_plot[:, 0], data_plot[:, 4])
    # plt.plot(data_plot[:, 0], data_plot[:, 4], 'ko', markersize=3)


    colors = iter(plt.cm.rainbow(np.linspace(0, 1, flights_number)))
    flight_folder = './data/gen/'+GENERATION_TYPE+'/'+PDF_TYPE+'/'+str(flights_number)+'-flights/'
    flight_data = genfromtxt(flight_folder+'data'+str(day_number)+'-'+str(flights_number)+'.csv', delimiter=',')

    for i in range(1, flights_number+1):
        col = next(colors)
        # TAKE ONLY THE flight passengers
        f_data = flight_data[np.where(flight_data[:, 2] == float(i))]
        d_plot = data_plot[np.in1d(data_plot[:, 0], f_data[:, 0]), :]
        plt.plot(d_plot[:, 0], d_plot[:, 4], 'o', color=col, markersize=3, label=i)


    lgd = plt.legend(loc='center left', bbox_to_anchor=(1, 0.5), numpoints=1, title='flight')
    # ARROWS IF SERVING > or <
    serving_data_plot = genfromtxt(folder_output+'var-agents/day-'+str(day_number)+'/serving-agents.csv', delimiter='\t')
    changes = serving_data_plot[:, 1]
    changes = np.roll(changes - np.roll(changes, -1), 1)
    changes[0] = 0
    serv_times_to_plot_minus  = serving_data_plot[:, 0][changes > 0]
    serv_times_to_plot_plus = serving_data_plot[:, 0][changes < 0]

    _col = plt.cm.rainbow(np.linspace(0, 1, flights_number))
    h = plt.ylim()[1]*0.9  #np.max(data_plot[:, 4])
    l = h*0.05
    for i in serv_times_to_plot_plus:
        plt.arrow(i, h, 0, l, head_width=l*5, head_length=l/10, fc='k', color=_col[1], shape='full')
    for i in serv_times_to_plot_minus:
        plt.arrow(i, h, 0, -l, head_width=l*5, head_length=l/10, fc='k', color=_col[-1], shape='full')


    flight_dep = genfromtxt(flight_folder+'data'+str(day_number)+'-'+str(flights_number)+'-info.csv', delimiter=',')
    colors = iter(_col)
    for i in range(1, flights_number+1):
        col = next(colors)
        # drow flight dep time
        plt.annotate(str(i), xy=(flight_dep[i-1], h*0.6),  xycoords='data',
            xytext=(-20, 30), textcoords='offset points',
            arrowprops=dict(arrowstyle="->",
            connectionstyle="angle,angleA=0,angleB=90,rad=10"),
            size=8
            )


    w_data = genfromtxt(folder_output+'var-agents/day-'+str(day_number)+'/mean_wait_times.csv', delimiter='\t')
    # print w_data
    # print np.in1d(data_plot[:, 0], w_data[:, 0])
    # print data_plot.shape

    #fit and overplot a 2nd order polynomial
    # x = data_plot[:, 0]
    # y = data_plot[:, 4]
    # params = np.polyfit(x, y, 3)
    # xp = data_plot[:, 0]#np.linspace(x.min(), x.max(), 200)
    # yp = np.polyval(params, xp)
    # plt.plot(xp, yp, 'k')
    #
    # #overplot an error band
    # sig = np.std(y - yp)
    # plt.fill_between(xp, yp - sig, yp + sig,
    #                  color='k', alpha=0.2)

    #SAVE
    plt.savefig(folder_output+'overall_queue_waiting_times_over_time_var_agents_day'+str(day_number)+'.png',
                bbox_extra_artists=(lgd,), bbox_inches='tight', dpi=150)
    plt.clf()


def plot_single_queue_waiting_times_over_times(flights_number, day_number, flight):
    print "\t\t\t\tplot_single_queue_waiting_times_over_times..."
    # folder_output = './data/results/'+GENERATION_TYPE+'/'+PDF_TYPE+'/'+str(flights_number)+'-flights/'
    folder_output = './data/results/'+GENERATION_TYPE+'/'+PDF_TYPE+'/'+str(flights_number)+'-flights/'

    plt.title('Single queue waiting times over time - day '+ str(day_number)+' flight:' + str(flight))
    plt.xlabel('T sim')
    plt.ylabel('Waiting times')
    plt.grid(True)

    # TAKE ONLY THE flight passengers
    flight_folder = './data/gen/'+GENERATION_TYPE+'/'+PDF_TYPE+'/'+str(flights_number)+'-flights/'
    flight_data = genfromtxt(flight_folder+'data'+str(day_number)+'-'+str(flights_number)+'.csv', delimiter=',')
    flight_data = flight_data[np.where(flight_data[:, 2] == float(flight))]


    # HISTOGRAM
    data_plot = genfromtxt(folder_output+'var-agents/day-'+str(day_number)+'/data'+str(day_number)+'-'+str(flights_number)+'-results.csv', delimiter='\t')
    # FILTER BY FLIGHT
    data_plot = data_plot[np.in1d(data_plot[:, 0], flight_data[:, 0]), :]
    plt.plot(data_plot[:, 0], data_plot[:, 4], 'ko', markersize=3)

    # ARROWS IF SERVING > or <
    serving_data_plot = genfromtxt(folder_output+'var-agents/day-'+str(day_number)+'/serving-agents.csv', delimiter='\t')
    changes = serving_data_plot[:, 1]
    changes = np.roll(changes - np.roll(changes, -1), 1)
    changes[0] = 0
    serv_times_to_plot_minus  = serving_data_plot[:, 0][changes > 0]
    serv_times_to_plot_plus = serving_data_plot[:, 0][changes < 0]
    h = plt.ylim()[1]*0.9
    l = h*0.05
    max_time = np.max(data_plot[:, 0])
    min_time = np.min(data_plot[:, 0])
    for i in serv_times_to_plot_plus:
        if i < max_time and i > min_time:
            plt.arrow(i, h, 0, l, head_width=l*5, head_length=l/10, fc='k', color='b', shape='full')
    for i in serv_times_to_plot_minus:
        if i < max_time and i > min_time:
            plt.arrow(i, h, 0, -l, head_width=l*5, head_length=l/10, fc='k', color='r', shape='full')


    #SAVE
    plt.savefig(folder_output+'single_queue_waiting_times_over_time_var_agents_day'+str(day_number)+'-flight-'+str(flight)+'.png')
    plt.clf()
    #
    #


def plot_number_sa_over_times(flights_number, day_number):
    # HISTOGRAM OF SERVING AGENTS
    print "\t\t\t\tplot_number_sa_over_times..."
    folder_output = './data/results/'+GENERATION_TYPE+'/'+PDF_TYPE+'/'+str(flights_number)+'-flights/'
    # folder_output = './data/results/'+GENERATION_TYPE+'/'+PDF_TYPE+'/'+str(flights_number)+'-flights/day-5/'

    plt.title('Serving agents over time')
    plt.xlabel('t [s]')
    plt.ylabel('#SA')
    plt.grid(True)


    c = '#186dec'
    data_plot = genfromtxt(folder_output+'var-agents/day-'+str(day_number)+'/serving-agents.csv', delimiter='\t')
    b = plt.bar(data_plot[:, 0], data_plot[:, 1], width=0.5, color=c, ecolor='b')
    plt.xlim((0, np.max(data_plot[:, 0])))
    plt.ylim((0, np.max(data_plot[:, 1])+1))

    for i in b:
        i.set_color(c)

    plt.savefig(folder_output+'number_sa_over_time_day'+str(day_number)+'.png', dpi=150)

    plt.clf()


# NOT USED ANYMORE
def plot_flights_lost_ratio(day_number):
    # USELESS !!!
    print "\t\t\t\tplot_flights_lost_ratio..."
    folder_output = './data/results/'
    url = './data/results/simulation-summary-results2.csv'
    # folder_output = './data/results/'+GENERATION_TYPE+'/'+PDF_TYPE+'/'+str(flights_number)+'-flights/day-5/'

    plt.title('Number of SA over time')
    plt.xlabel('T sim')
    plt.ylabel('#Serving Agents')
    plt.grid(True)

    input_raw = []
    with open(url, 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter='\t')
        for row in spamreader:
            input_raw.append(row)


    data_plot = np.zeros((len(input_raw)/2, len(input_raw[0])-1))

    i = 0

    for entry in input_raw:
        if entry[-1] == 'gaussian':
            if entry[-2] == 'var':
                entry[-2] = 0
            data_plot[i, :] = np.array(entry[:-1])
            i += 1


    data_plot = data_plot[np.where(data_plot[:, 0] == day_number)]
    data_plot = data_plot[np.where(data_plot[:, 5] > 0)]

    # remove NaN
    # data_plot[np.isnan(data_plot)] = 0

    colors = iter(plt.cm.rainbow(np.linspace(0, 1, 12)))
    col = next(colors)

    for label in set(data_plot[:, 1]):
        if label != 1:
            data = data_plot[np.where(data_plot[:, 1] == label)]
            plt.plot(data[:, 5], data[:, 4]/100, label=label, color=col)
        # print data[:, 4]
        # print data.shape
        # print data
            col = next(colors)

    plt.legend(loc=1)
    plt.savefig(folder_output+'number_sa_over_time.png')

    plt.clf()


def plot_interarrival(day):


    # TAKE ONLY THE flight passengers
    flight_folder = './data/gen/'+GENERATION_TYPE+'/'+PDF_TYPE+'/10-flights/'
    flight_data = genfromtxt(flight_folder+'data'+str(day)+'-10.csv', delimiter=',')
    flight_data = flight_data[:, 0]

    diff = np.roll(flight_data, -1) - flight_data
    diff = diff[:-1] # remove last value
    plt.hist(diff, bins=(np.max(diff)-np.min(diff)))

    # HISTOGRAM OF SERVING AGENTS
    print "\t\t\t\tplot_interarrival..."
    folder_output = './data/results/'

    plt.title('Interarrival')
    plt.xlabel('t [s]')
    plt.ylabel('#interarrival')
    plt.grid(True)
    plt.xlim((0, 90))
    # plt.ylim((0, 180))


    # plt.show()
    plt.savefig(folder_output+'interarrival'+str(day)+'.png')

    plt.clf()



def plot_data_distribution(day_number):
    print "\t\t\tplot_data_distribution("+str(day_number)+")"
    # TAKE ONLY THE flight passengers
    flight_folder = './data/gen/'+GENERATION_TYPE+'/'+PDF_TYPE+'/10-flights/'
    data = genfromtxt(flight_folder+'data'+str(day_number)+'-10.csv', delimiter=',')
    data = data[:, 0]



    plt.title('Queue popolation - day '+str(day_number))
    plt.xlabel('t [s]')
    plt.ylabel('#passengers')
    plt.grid(True)
    plt.ylim((0, 70))

    plt.hist(data, bins=np.max(data)/(60*5), color='k')

    folder_output = './data/results/'
    plt.savefig(folder_output+'day-'+str(day_number)+'-histogram.png', dpi=150)

    plt.clf()

def plot_flight_distribution(flights_number, day_number):
    print "\t\t\t\tplot_flight_distribution..."
    # folder_output = './data/results/'+GENERATION_TYPE+'/'+PDF_TYPE+'/'+str(flights_number)+'-flights/'
    # folder_output = './data/results/'+GENERATION_TYPE+'/'+PDF_TYPE+'/'+str(flights_number)+'-flights/'
    PDF_TYPE = 'rayleigh'
    plt.title('Overall queue waiting times over time - day '+str(day_number) + ' - ' + PDF_TYPE + ' distribution')
    plt.xlabel('T sim')
    plt.ylabel('flight')
    plt.grid(True)

    # HISTOGRAM
    # data_plot = genfromtxt(folder_output+'var-agents/day-'+str(day_number)+'/data'+str(day_number)+'-'+str(flights_number)+'-results.csv', delimiter='\t')
    # plt.bar(data_plot[:, 0], data_plot[:, 4])
    # plt.plot(data_plot[:, 0], data_plot[:, 4], 'ko', markersize=3)


    colors = iter(plt.cm.rainbow(np.linspace(0, 1, flights_number)))
    flight_folder = './data/gen/'+GENERATION_TYPE+'/'+PDF_TYPE+'/'+str(flights_number)+'-flights/'
    flight_data = genfromtxt(flight_folder+'data'+str(day_number)+'-'+str(flights_number)+'.csv', delimiter=',')

    for i in range(1, flights_number+1):
        col = next(colors)
        # TAKE ONLY THE flight passengers
        f_data = flight_data[np.where(flight_data[:, 2] == float(i))]
        # d_plot = data_plot[np.in1d(data_plot[:, 0], f_data[:, 0]), :]
        plt.plot(f_data[:, 0], f_data[:, 2], 'x', color=col, markersize=3, label=i)


    lgd = plt.legend(loc='center left', bbox_to_anchor=(1, 0.5), numpoints=1, title='flight')
    _col = plt.cm.rainbow(np.linspace(0, 1, flights_number))

    flight_dep = genfromtxt(flight_folder+'data'+str(day_number)+'-'+str(flights_number)+'-info.csv', delimiter=',')
    colors = iter(_col)
    for i in range(1, flights_number+1):
        col = next(colors)
        # drow flight dep time
        plt.annotate(str(i)+' closure time', xy=(flight_dep[i-1], i),  xycoords='data',
            xytext=(+20, 10), textcoords='offset points',
            arrowprops=dict(arrowstyle="->",
            connectionstyle="angle,angleA=0,angleB=90,rad=10"),
            size=8
            )
    #SAVE
    plt.ylim((0, flights_number+1))
    plt.savefig(flight_folder+'overall_passenger_flight_distribution.png',
                bbox_extra_artists=(lgd,), bbox_inches='tight', dpi=150)
    plt.clf()


def cost(x, y):
    # x = xy[0]
    # y = xy[1]
    return (1 + x*4 + y*3)**2


if __name__ == "__main__":
    print "\t\t\tSaving plots..."

    day = 5
    n_flights = 10
    flight_to_study = 10
    agent_to_study = 0 # for var

    # 1
    # plot_overall_agents_queue_length_over_time(n_flights, day)

    # 2
    # plot_single_agent_queue_length_over_time(n_flights, day, agent_to_study)

    # 3 NO
    # plot_overall_queue_waiting_times_over_times(n_flights, day)

    # 4 NO
    # for i in range(1, n_flights+1):
    #     plot_single_queue_waiting_times_over_times(n_flights, day, i)

    # 5
    plot_number_sa_over_times(n_flights, day)

    # 6
    # plot_interarrival(day)

    # STOP NON SERVE plot_flights_lost_ratio(5)

    # 7
    # for i in range(1, 6):
    #     plot_data_distribution(i)

    # 8
    # plot_flight_distribution(10, 5)





